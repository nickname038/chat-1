import React from "react";
import "./Chat.css";
import Header from "./Header/Header";
import MessageInput from "./MessageInput/MessageInput";
import MessageList from "./MessageList/MessageList";
import Preloader from "./Preloader/Preloader";

const myUserId = "my-id";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isChanging: { value: false, id: undefined } };
    this.onCreateMessage = this.onCreateMessage.bind(this);
    this.onChangeMessage = this.onChangeMessage.bind(this);
    this.resetChengingFlag = this.resetChengingFlag.bind(this);
    this.onDeleteMessage = this.onDeleteMessage.bind(this);
    this.onLikeMessage = this.onLikeMessage.bind(this);
  }

  resetChengingFlag(isMessageChange, id) {
    this.setState({ isChanging: { value: isMessageChange, id } });
  }

  getCurrentDate() {
    const dateMs = new Date();
    const dateStr = dateMs.toISOString();
    return dateStr;
  }

  onDeleteMessage(id) {
    const newMessages = this.state.messages.filter(
      (message) => message.id !== id
    );
    const users = [];
    newMessages.forEach((message) => {
      if (!users.includes(message.user)) {
        users.push(message.user);
      }
    });

    this.setState({
      messages: newMessages,
      messagesCount: this.state.messagesCount - 1,
      usersCount: users.length,
    });
  }

  onLikeMessage(id) {
    const newMessages = this.state.messages.slice();
    this.setState({
      messages: newMessages.map((message) => {
        if (message.id === id) {
          message.isLiked = !message.isLiked;
        }
        return message;
      }),
    });
  }

  onCreateMessage(text) {
    const newMessages = this.state.messages.slice();
    const date = this.getCurrentDate();
    newMessages.push({
      avatar:
        "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      createdAt: date,
      editedAt: "",
      id: date,
      text,
      user: "Yuliia",
      userId: myUserId,
    });
    const users = [];
    newMessages.forEach((message) => {
      if (!users.includes(message.user)) {
        users.push(message.user);
      }
    });

    this.setState({
      messages: newMessages,
      messagesCount: this.state.messagesCount + 1,
      usersCount: users.length,
    });
  }

  onChangeMessage(text) {
    const newMessages = this.state.messages.slice();
    newMessages.map((message) => {
      if (message.id === this.state.isChanging.id) {
        message.text = text;
      }
      return message;
    });
    this.setState({
      messages: newMessages,
    });
  }

  render() {
    if (!this.state.messages) {
      fetch(this.props.url)
        .then((response) => response.json())
        .then((messages) => {
          const users = [];
          messages.forEach((message) => {
            if (!users.includes(message.user)) {
              users.push(message.user);
            }
          });
          this.isLoad = true;
          this.setState({
            messages,
            usersCount: users.length,
            messagesCount: messages.length,
          });
        });
    }
    return (
      <div className="chat">
        {this.isLoad ? (
          <div className="chat-components-wrapper">
            <Header
              usersCount={this.state.usersCount}
              messagesCount={this.state.messagesCount}
              lastMessage={this.state.messages[this.state.messages.length - 1]}
            />
            <MessageList
              messages={this.state.messages}
              myUserId={myUserId}
              resetChengingFlag={this.resetChengingFlag}
              onDeleteMessage={this.onDeleteMessage}
              onLikeMessage={this.onLikeMessage}
            />
            <MessageInput
              onCreateMessage={this.onCreateMessage}
              onChangeMessage={this.onChangeMessage}
              resetChengingFlag={this.resetChengingFlag}
              isChanging={this.state.isChanging}
            />
          </div>
        ) : (
          <Preloader />
        )}
      </div>
    );
  }
}

export default Chat;
