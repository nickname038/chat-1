import React from "react";
import "./Header.css";

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="header-info-wrapper">
          <h1 className="header-title">My chat</h1>
          <span className="header-users-count">{this.props.usersCount} participants</span>
          <span className="header-messages-count">{this.props.messagesCount}</span>
          <span>  messages</span>
        </div>
        <span className="header-last-message-date">
          {this.props.lastMessage ? `Last messages ${this.props.lastMessage.createdAt.substr(0, 10)}` : ""}
        </span>
      </div>
    );
  }
}

export default Header;
