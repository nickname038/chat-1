import React from "react";
import "./MessageList.css";
import Message from "./Message/Message";
import OwnMessage from "./Message/OwnMessage/OwnMessage";

let currData;
let newData;
let isNewDayMessage;

class MessageList extends React.Component {
  render() {
    const renderMessages = this.props.messages.map((message, i) => {
      newData = message.createdAt.substr(0, 10);
      isNewDayMessage = currData !== newData;
      currData = newData;
      if (message.userId === this.props.myUserId) {
        return (
          <OwnMessage
            message={message}
            isNewDayMessage={isNewDayMessage}
            date={currData}
            onChangeMessage={this.props.onChangeMessage}
            resetChengingFlag={this.props.resetChengingFlag}
            onDeleteMessage={this.props.onDeleteMessage}
          />
        );
      }
      return (
        <Message
          message={message}
          isNewDayMessage={isNewDayMessage}
          date={currData}
          onLikeMessage={this.props.onLikeMessage}
        />
      );
    });
    return (
      <div className="message-wrapper">
        <div className="message-list">
          {renderMessages}
        </div>
      </div>
    );
  }
}

export default MessageList;
