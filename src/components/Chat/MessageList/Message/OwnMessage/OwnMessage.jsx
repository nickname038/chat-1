import React from "react";
import "./OwnMessage.css";
import MessageMain from "../MessageMain/MessageMain";
import MessageDevider from "../../MessageDevider/MessageDevider";

class OwnMessage extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  onDelete() {
    this.props.onDeleteMessage(this.props.message.id);
  }

  onChange(e) {
    const textarea = document.querySelector("textarea");
    textarea.value = this.props.message.text;

    this.props.resetChengingFlag(true, this.props.message.id);
  }

  render() {
    return (
      <div className="message-devider-wrapper">
        {this.props.isNewDayMessage && (
          <MessageDevider date={this.props.date} />
        )}
        <div className="own-message">
          <MessageMain
            text={this.props.message.text}
            time={this.props.message.createdAt}
          />
          <div className="message-footer">
            <button className="message-edit" onClick={this.onChange}>Edit</button>
            <button className="message-delete" onClick={this.onDelete}>Delete</button>
          </div>
        </div>
      </div>
    );
  }
}

export default OwnMessage;
