import React from "react";
import "./MessageMain.css";

class MessageMain extends React.Component {
  render() {
    return (
      <div className="message-main">
        <span className="message-time">{this.props.time.substr(11, 5)}</span>
        <span className="message-text">{this.props.text}</span>
      </div>
    );
  }
}

export default MessageMain;
