import React from "react";
import "./MessageDevider.css";

class MessageDevider extends React.Component {
  render() {
    return <div className="messages-divider">{this.props.date}</div>;
  }
}

export default MessageDevider;
