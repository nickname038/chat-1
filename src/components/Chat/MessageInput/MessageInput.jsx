import React from "react";
import "./MessageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    const textarea = document.querySelector("textarea");
    const text = textarea.value;
    textarea.value = "";

    if (this.props.isChanging.value) {
      this.props.onChangeMessage(text);
      this.props.resetChengingFlag(false);
    } else {
      this.props.onCreateMessage(text);
    }
  }

  render() {
    return (
      <div className="message-input">
        <form>
          <textarea
            className="message-input-text"
            placeholder="Message"
          ></textarea>
          <button
            type="button"
            className="message-input-button"
            onClick={this.onClick}
          >
            {this.props.isChanging.value ? "Change" : "Send"}
          </button>
        </form>
      </div>
    );
  }
}

export default MessageInput;
